import { initializeApp } from "firebase/app";
import { getFirestore } from "@firebase/firestore";
import { getAuth } from 'firebase/auth'

const firebaseConfig = {
  apiKey: "AIzaSyAQOQa6SjB-nTo2Q40Us57HZVW7LWIbMkI",
  authDomain: "fir-tut-72dfe.firebaseapp.com",
  projectId: "fir-tut-72dfe",
  storageBucket: "fir-tut-72dfe.appspot.com",
  messagingSenderId: "769411539680",
  appId: "1:769411539680:web:8ddf3e97240174f3ade685",
  measurementId: "G-9MZNN6WM6W"
  };

const app = initializeApp(firebaseConfig);

export const auth = getAuth(app)
export const db = getFirestore(app);

