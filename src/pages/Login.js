import React, { Component, useState } from 'react';
import { signInWithEmailAndPassword } from 'firebase/auth';
import { Container, Form, Row, Col } from 'react-bootstrap';
import { Label, Input, Button} from 'reactstrap';
import { auth } from "../services/firebase-config";
import { useNavigate } from "react-router-dom";
import './Login.css';

const Login = () => {

	const [loginEmail, setLoginEmail] = useState("");
	const [loginPassword, setLoginPassword] = useState("");

	const login = async () => {
		// console.log("loginEmail: " + loginEmail + " loginPass: " + loginPassword)
		try {
		  const user = await signInWithEmailAndPassword(
			auth,
			loginEmail,
			loginPassword
		  );
		  console.log(user);
		  backHome();
		} catch (error) {
		  console.log(error.message);
		}
	  };

	  let navigate = useNavigate(); 
  	const backHome = () =>{ 
    let path = `/`; 
    navigate(path);
  }


	return (
		<div>
			<Container className="nama">
				<Row>
					<Col lg={4} md={6} sm={12} className="box p-5 m-auto shadow-sm">
						<h1 className="login"> Sign In</h1>
						<Form> 
							<Form.Group className="label mb-3" controlId="formBasicEmail">
							<Label for="Email">Email Address</Label>
          					<Input value={loginEmail} onChange={(e) => setLoginEmail(e.target.value)} name='email' type='email' autoComplete='email' required/>
							</Form.Group>

							<Form.Group className="label mb-3" controlId="formBasicPassword">
							<Label for="Password">Password</Label>
          					<Input value={loginPassword} onChange={(e) => setLoginPassword(e.target.value)} name='password' type='password' autoComplete='password' required/>
							</Form.Group>

							<Button variant="primary" onClick={login}>
								Submit
							</Button>
						</Form>
					</Col>
				</Row>
			</Container>
		</div>
	);
};

export default Login;
